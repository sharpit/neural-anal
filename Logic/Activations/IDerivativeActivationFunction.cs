namespace SharpIT.NeuralNetworks.Activations
{
    public interface IDerivativeActivationFunction : IActivationFunction
    {
        double GetDerivative(double x);
    }
}