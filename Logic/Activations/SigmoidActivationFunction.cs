﻿using System;

namespace SharpIT.NeuralNetworks.Activations
{
    public class SigmoidActivationFunction : IDerivativeActivationFunction
    {
        public double Calculate(double x)
        {
            return 1.0d / (1.0d + Math.Exp(-x));
        }
        
        public double GetDerivative(double x)
        {
            return x * (1 - x);
        }
    }
}