namespace SharpIT.NeuralNetworks.Activations
{
    public interface IActivationFunction
    {
        /// <summary>
        /// Liczy warto�� funkcji aktywacji
        /// </summary>
        /// <param name="x">argument</param>
        /// <returns></returns>
        double Calculate(double x);
    }
}