﻿using System.Collections.Generic;

namespace SharpIT.NeuralNetworks.Training
{
    public class TrainingData
    {
        /// <summary>
        /// Próbki
        /// </summary>
        public IList<TrainingSample> Samples { get; private set; }

        public TrainingData()
        {
            Samples = new List<TrainingSample>();
        }
    }
}
