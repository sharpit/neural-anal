﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SharpIT.NeuralNetworks.Activations;
using SharpIT.NeuralNetworks.Models;

namespace SharpIT.NeuralNetworks.Training
{
    public class NetworkTrainer
    {
        protected readonly Network _network;

        protected readonly IDerivativeActivationFunction _derivativeFunction;

        /// <summary>
        /// maksymalna liczba iteracji
        /// </summary>
        protected readonly int _maxEpoch;

        protected readonly double _acceptableError;

        protected readonly double _learningRate;

        public NetworkTrainer(
            Network network,
            IDerivativeActivationFunction derivativeFunction,
            int maxEpoch,
            double acceptableError,
            double learningRate)
        {
            _network = network;
            _derivativeFunction = derivativeFunction;
            _maxEpoch = maxEpoch;
            _acceptableError = acceptableError;
            _learningRate = learningRate;
        }

        /// <summary>
        /// Przeprowadź proces uczenia się
        /// </summary>
        public TrainingResult Train(TrainingData pattern)
        {
            var result = new TrainingResult();
            var sw = Stopwatch.StartNew();

            do
            {
                result.TotalError = 0;

                foreach (var sample in pattern.Samples)
                {
                    var layer = _network.Layers.First(x => x.Type == LayerType.Inputs);
                    layer.Neurons[0].Output = sample.Inputs[0];
                    layer.Neurons[1].Output = sample.Inputs[1];

                    layer = _network.Layers.First(x => x.Type == LayerType.Outputs);
                    layer.Neurons[0].Output = sample.Outputs[0];

                    FeedForward();
                    result.TotalError += BackPropagate();
                    UpdateWeights();
                }

                if (result.Epochs % 1000 == 0)
                {
                    // Debug.Print(result.TotalError.ToString());
                }

            } while (++result.Epochs < _maxEpoch && result.TotalError > _acceptableError);

            sw.Stop();
            result.Duration = sw.Elapsed;
            return result;
        }

        /// <summary>
        /// Zapytaj o wynik nauczonej sieci
        /// </summary>
        public IEnumerable<double> Ask(IEnumerable<double> input)
        {
            var inputs = input.ToArray();
            var layer = _network.Layers.First(x => x.Type == LayerType.Inputs);
            layer.Neurons[0].Output = inputs[0];
            layer.Neurons[1].Output = inputs[1];

            FeedForward();

            layer = _network.Layers.First(x => x.Type == LayerType.Outputs);

            return layer.Neurons.Select(neuron => neuron.Output);
        }

        /// <summary>
        /// Forward propagate
        /// </summary>
        protected void FeedForward()
        {
            foreach (var layer in _network.Layers)
            {
                if (layer.Type != LayerType.Inputs)
                {
                    foreach (var neuron in layer.Neurons)
                    {
                        neuron.Activate();
                    }
                }
            }
        }

        protected double BackPropagate()
        {
            var totalError = double.MaxValue;

            for (var i = _network.Layers.Count - 1; i >= 0; i--)
            {
                var layer = _network.Layers[i];

                if (layer.Type == LayerType.Outputs)
                {
                    // błąd całkowity (cost function)
                    totalError = layer.Neurons.Sum(x => Math.Pow(x.Expected - x.Output, 2) / 2.0d);
                }

                foreach (var neuron in layer.Neurons)
                {
                    var gradient = _derivativeFunction.GetDerivative(neuron.Output);

                    if (layer.Type == LayerType.Outputs)
                    {
                        neuron.Error = (neuron.Expected - neuron.Output) * gradient;
                    }
                    else
                    {
                        var outputConnections = neuron.OutputConnections;
                        neuron.Error = outputConnections.Sum(x => x.Target.Error * x.Weight) * gradient;
                    }
                }
            }

            return totalError;
        }

        protected void UpdateWeights()
        {
            foreach (var layer in _network.Layers)
            {
                foreach (var neuron in layer.Neurons)
                {
                    foreach (var synapse in neuron.OutputConnections)
                    {
                        synapse.Weight -= _learningRate * neuron.Output * neuron.Error;
                        //synapse.Weight -= _learningRate * neuron.Error;
                    }
                }
            }
        }
    }
}
