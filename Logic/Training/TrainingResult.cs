﻿using System;

namespace SharpIT.NeuralNetworks.Training
{
    public class TrainingResult
    {
        public int Epochs { get; set; }
        public double TotalError { get; set; }
        public TimeSpan Duration { get; set; }
    }
}