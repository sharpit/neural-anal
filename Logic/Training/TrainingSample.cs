using System.Collections.Generic;

namespace SharpIT.NeuralNetworks.Training
{
    public class TrainingSample
    {
        /// <summary>
        /// Dane wej�ciowe
        /// </summary>
        public IList<double> Inputs { get; private set; }

        /// <summary>
        /// Oczekiwane rezultaty
        /// </summary>
        public IList<double> Outputs { get; private set; }

        public TrainingSample()
        {
            Inputs = new List<double>();
            Outputs = new List<double>();
        }
    }
}