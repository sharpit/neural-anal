﻿using System;

namespace SharpIT.NeuralNetworks
{
    public static class RandomDataGenerator
    {
        private static readonly Random Randomizer = new Random(GetSeed());

        public static int GetSeed()
        {
            unchecked
            {
                return (int)DateTime.Now.Ticks;
            }
        }

        public static double GetInRange(double min, double max)
        {
            var d = Randomizer.NextDouble();
            var range = Math.Abs(max - min) + 1;
            return min + d * range;
        }


    }
}
