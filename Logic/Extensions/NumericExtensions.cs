﻿// ReSharper disable once CheckNamespace
namespace System
// ReSharper disable once CheckNamespace
{
    public static class NumericExtensions
    {
        public static bool AreClose(this decimal d1, decimal d2, decimal epsilon)
        {
            return Math.Abs(d1 - d2) <= epsilon;
        }

        public static bool AreClose(this double d1, double d2, double epsilon)
        {
            return Math.Abs(d1 - d2) <= epsilon;
        }


    }
}
