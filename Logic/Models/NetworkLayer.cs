﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SharpIT.NeuralNetworks.Models
{
    /// <summary>
    /// Warstwa neuronów
    /// </summary>
    [DebuggerDisplay("Neurons = {Neurons.Count}, Type = {Type}")]
    public class NetworkLayer
    {
        /// <summary>
        /// Neurony w tej warstwie
        /// </summary>
        public IList<Neuron> Neurons { get; private set; }

        /// <summary>
        /// Następna warstwa
        /// </summary>
        public NetworkLayer Next { get; private set; }

        /// <summary>
        /// typ warstwy
        /// </summary>
        public LayerType Type { get; set; }

        public NetworkLayer()
            : this(LayerType.Hidden)
        {
        }

        public NetworkLayer(LayerType type)
        {
            Neurons = new List<Neuron>();
            Type = type;
        }

        /// <summary>
        /// Połączenie tej warstwy z warstwą kolejną
        /// Każdy neuron z tej warstwy będzie połączony z każdy neuronem warstwy następującej
        /// </summary>
        /// <param name="nextLayer">Warstwa kolejna</param>
        public void SetNext(NetworkLayer nextLayer)
        {
            if (nextLayer == null)
            {
                throw new ArgumentNullException("nextLayer");
            }

            if (Next == nextLayer)
            {
                throw new InvalidOperationException("Podana warstwa już jest ustawiona jako warstwa następująca");
            }

            var query = from s in Neurons from t in nextLayer.Neurons select new { s, t };

            foreach (var pair in query)
            {
                var synapse = new Synapse(pair.s, pair.t);

                pair.s.Synapses.Add(synapse);
                pair.t.Synapses.Add(synapse);
            }

            nextLayer.Next = nextLayer;
        }

    }
}