﻿using System.Diagnostics;

namespace SharpIT.NeuralNetworks.Models
{
    /// <summary>
    /// Neuron connection
    /// </summary>
    [DebuggerDisplay("weight = {Weight}")]
    public class Synapse
    {
        /// <summary>
        /// Source neuron
        /// </summary>
        public Neuron Source { get; set; }

        /// <summary>
        /// Target neuron
        /// </summary>
        public Neuron Target { get; set; }

        /// <summary>
        /// Weight value
        /// </summary>
        public double Weight { get; set; }

        public Synapse(Neuron source, Neuron target)
            : this(source, target, RandomDataGenerator.GetInRange(0.0d, 1.0d))
        {
        }

        public Synapse(Neuron source, Neuron target, double weight)
        {
            Source = source;
            Target = target;
            Weight = weight;
        }
    }
}