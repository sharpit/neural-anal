﻿namespace SharpIT.NeuralNetworks.Models
{
    public enum LayerType
    {
        Inputs,
        Hidden,
        Outputs
    }
}