﻿namespace SharpIT.NeuralNetworks.Models
{
    public class NeuronInput
    {
        public double Value { get; set; }
        public double Weight { get; set; }
    }
}