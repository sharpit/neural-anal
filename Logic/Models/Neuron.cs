﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SharpIT.NeuralNetworks.Activations;

namespace SharpIT.NeuralNetworks.Models
{
    [DebuggerDisplay("Id# {Id}, Bias = {Bias}, Output = {Output}")]
    public class Neuron
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public Guid Id { get; private set; }

        /// <summary>
        /// Akson - output value
        /// </summary>
        public double Output { get; set; }

        /// <summary>
        /// Expected value
        /// </summary>
        public double Expected { get; set; }

        /// <summary>
        /// Bias
        /// </summary>
        public double Bias { get; set; }

        /// <summary>
        /// Error value
        /// </summary>
        public double Error { get; set; }

        /// <summary>
        /// Connections to other neurons
        /// synapsy
        /// </summary>
        public IList<Synapse> Synapses { get; private set; }

        /// <summary>
        /// Input connections 
        /// </summary>
        public IEnumerable<Synapse> InputConnections
        {
            get { return Synapses.Where(x => x.Target == this); }
        }

        /// <summary>
        /// Output connections
        /// </summary>
        public IEnumerable<Synapse> OutputConnections
        {
            get { return Synapses.Where(x => x.Source == this); }
        }

        protected readonly IActivationFunction _activationFunction;

        public Neuron(IActivationFunction activationFunction)
        {
            Id = Guid.NewGuid();
            Synapses = new List<Synapse>();
            Bias = 1.0d;
            _activationFunction = activationFunction;
        }

        public double Activate()
        {
            var z = InputConnections.Sum(x => x.Source.Output * x.Weight);
            var a = _activationFunction.Calculate(z);
            Output = a;
            return a;
        }
        
    }
}