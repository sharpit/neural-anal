﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SharpIT.NeuralNetworks.Models
{
    [DebuggerDisplay("Layers = {Layers.Count}")]
    public class Network
    {
        public IList<NetworkLayer> Layers { get; private set; }
        public IList<NeuronInput> Inputs { get; private set; }

        public Network()
        {
            Layers = new List<NetworkLayer>();
            Inputs = new List<NeuronInput>();
        }
        
        public void AddLayer(NetworkLayer layer)
        {
            if (layer == null)
            {
                throw new ArgumentNullException("layer");
            }

            if (Layers.Contains(layer))
            {
                throw new InvalidOperationException("podana warstwa została już dodana");
            }

            Layers.Add(layer);
        }
    }
}
