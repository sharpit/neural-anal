﻿using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using SharpIT.NeuralNetworks.Activations;
using SharpIT.NeuralNetworks.Models;
using SharpIT.NeuralNetworks.Training;

namespace SharpIT.NeuralNetworks.Tests
{
    [TestFixture]
    public class NeuralNetworksTests
    {
        [Test]
        public void XorLearningTest()
        {
            // arrange
            var network = new Network();
            var acceptableError = 1e-3;
            var activationFunction = new SigmoidActivationFunction();
            var maxIterations = 1000000;
            var learningRate = 0.2d;
            var networkTrainer = new NetworkTrainer(network, activationFunction, maxIterations, acceptableError, learningRate);

            // input layer
            var inputLayer = new NetworkLayer(LayerType.Inputs);
            var neuron = new Neuron(activationFunction);

            inputLayer.Neurons.Add(neuron);

            neuron = new Neuron(activationFunction);

            inputLayer.Neurons.Add(neuron);

            network.Layers.Add(inputLayer);

            // hidden layer
            var hiddenLayer = new NetworkLayer(LayerType.Hidden);
            neuron = new Neuron(activationFunction);
            hiddenLayer.Neurons.Add(neuron);

            neuron = new Neuron(activationFunction);
            hiddenLayer.Neurons.Add(neuron);

            network.Layers.Add(hiddenLayer);

            // ouput layer
            var outputLayer = new NetworkLayer(LayerType.Outputs);
            neuron = new Neuron(activationFunction);
            outputLayer.Neurons.Add(neuron);
            network.Layers.Add(outputLayer);

            // connections
            inputLayer.SetNext(hiddenLayer);
            hiddenLayer.SetNext(outputLayer);

            // act
            var trainingResult = networkTrainer.Train(GetTrainingData());
            Debug.Print("Trening: epochs = {0}, error = {1}, duration = {2} ms", trainingResult.Epochs, trainingResult.TotalError, trainingResult.Duration.TotalMilliseconds);

            foreach (var data in GetTrainingData().Samples)
            {
                var askResult = networkTrainer.Ask(data.Inputs).ToArray();
                Debug.Print("Result for [{0},{1}]: {2} = {3}", data.Inputs[0], data.Inputs[1], data.Outputs.First(), askResult.First());

                Assert.IsTrue(data.Outputs.First().AreClose(askResult.First(), 0.01d));
            }
        }

        protected TrainingData GetTrainingData()
        {
            var trainingData = new TrainingData();

            var sample = new TrainingSample();
            sample.Inputs.Add(0);
            sample.Inputs.Add(0);
            sample.Outputs.Add(0);
            trainingData.Samples.Add(sample);

            sample = new TrainingSample();
            sample.Inputs.Add(0);
            sample.Inputs.Add(1);
            sample.Outputs.Add(1);
            trainingData.Samples.Add(sample);

            sample = new TrainingSample();
            sample.Inputs.Add(1);
            sample.Inputs.Add(0);
            sample.Outputs.Add(1);
            trainingData.Samples.Add(sample);

            sample = new TrainingSample();
            sample.Inputs.Add(1);
            sample.Inputs.Add(1);
            sample.Outputs.Add(0);
            trainingData.Samples.Add(sample);

            return trainingData;
        }

    }


}
